import Map from "./Components/Page1";
import Page_1 from "./Components/Page1";
import { createAppContainer } from 'react-navigation';
// import { createStackNavigator } from 'react-navigation-tabs';
// import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createBottomTabNavigator } from "react-navigation-tabs";
import Coordonates from "./Components/Coordonates";
import Page_2 from "./Components/Page2";
// import Header from "./Components/Header";

const Routes = createBottomTabNavigator(
    {
        Carte: { screen: Page_1 },
        Test: { screen: Page_2 },
    }
);

export default createAppContainer(Routes);
