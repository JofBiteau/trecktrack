const initialState = {donnees: []};

function coordonatesReducers(state = initialState, action){
    let nextState;
    console.log("state");
    console.log(state);
    switch (action.type) {
        case ('lastPosition'):
            nextState = {
                ...state,
                donnees: [action.value]
            };
            break;
        default:
            nextState = state;
    };

    return nextState;
}

export default coordonatesReducers;
