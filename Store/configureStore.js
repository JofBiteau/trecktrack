import {createStore} from "redux";
import coordonatesReducers from "./Reducers/coordonatesReducer";

export default createStore(coordonatesReducers);
