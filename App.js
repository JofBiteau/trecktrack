import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import CoordonatesComponent from './Components/Coordonates';
import HeaderComponent from './Components/Header';
import Routes from "./routing";
import {styles} from './styles';
import {Provider} from "react-redux";
import Store from './Store/configureStore';


export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { message: "", nbTime: 0}
    }

    render() {
        return (
            <Provider store={Store} >
                <View style={styles.container}>
                    <Routes/>
                </View>
            </Provider>);
    };
};


