import React from 'react'
import { Text, View } from 'react-native'
import { styles } from "./styles";
import Header from "../Header";
import Coordonates from "../Coordonates";

class Page_2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = { message: ""}

    }

    getHistory = () => {
        //todo: get history
    }

    render(){
        return (
            <View style={styles.container}>
                <Text>Page_2</Text>
            </View>
        );
    }
}

export default Page_2;
