import React from 'react'
import { Button, Text, View } from 'react-native'
import { styles } from "./styles";
import Geolocation from '@react-native-community/geolocation';
import Alert from "react-native-web/src/exports/Alert";
import {connect} from "react-redux";


class Coordonates extends React.Component {
    constructor(props) {
        super(props);
        this.state = { date: new Date().toISOString(), location: {latitude: "0", longitude: "0"}, message: ""}
    }

    getPosition = () => {
        Geolocation.getCurrentPosition(
            info => this.setState({location: info.coords}),
            error => Alert.alert(error.message)
        );
    };

    onPressButton = () => {
        this.getPosition();

        const action = {type: "lastPosition", value: this.state.location };
        this.props.dispatch(action);

        this.setState({
            latitude: this.state.location.latitude,
            longitude: this.state.location.longitude,
            date: new Date().toISOString()
        });
    };

    render(){
        return (
            <View style={ styles.container }>
                <Text>Date: {this.state.date}</Text>
                <Text>Latitude: {this.state.latitude}</Text>
                <Text>Longitude: {this.state.longitude}</Text>
                <Text>{this.state.message}</Text>

                <Button title="Refresh coordonates" onPress={() => {
                    this.onPressButton();
                 }}/>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        donnees: state.donnees
    }
};

export default connect(mapStateToProps)(Coordonates);
