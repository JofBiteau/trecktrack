import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create( {
    container:{
        flex: 1,
        backgroundColor: '#383',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
