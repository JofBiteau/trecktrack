import React from 'react'
import { Text, View } from 'react-native'
import { styles } from "./styles";
import { connect } from "react-redux";
import MapView from "react-native-maps";

class Map extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initialPosition:{
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0
            },
            markerPosition: {
                latitude: 0,
                longitude: 0
            }
        }
    }

    render(){
        return (
            <View style={ styles.container }>
                <Text>component map {console.log(this.props.donnees[0])}</Text>
                <Text>test latitude: {this.props.donnees[0].latitude}</Text>
                {/*<MapView*/}
                {/*    initialRegion={this.props.donnees[0]}>*/}
                {/*    /!*<MapView.Marker*!/*/}
                {/*    /!*    coordonate={this.props.donnees[0]}>*!/*/}
                {/*    /!*</MapView.Marker>*!/*/}
                {/*</MapView>*/}
                <MapView
                    region={{
                        latitude: this.props.donnees[0].latitude,
                        longitude:  this.props.donnees[0].longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421
                    }}
                />

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        donnees: state.donnees
    }
};

export default connect(mapStateToProps)(Map);
