import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create( {
    container:{
        flex: 1,
        backgroundColor: '#f00',
    },
    styles_header: {
        flex: 1,
    },

    styles_main_content: {
        flex: 3,
        backgroundColor: '#383',
        alignItems: 'center',
        justifyContent: 'center',
    },

});
