import React from 'react'
import { Text, View } from 'react-native'
import { styles } from "./styles";
import Header from "../Header";
import Coordonates from "../Coordonates";
import Map from "../Map";

class Page_1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = { message: ""}

    }

    render(){
        return (
            <View style={styles.container}>
                <View style={styles.styles_header}>
                    <Header/>
                </View>

                <View style={styles.styles_main_content}>
                    {/*<Text> //TODO: MAP_HERE </Text>*/}
                    <Map/>
                </View>
            </View>
        );
    }
}

export default Page_1;
