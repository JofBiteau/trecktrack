import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
    },

    styles_header: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    styles_main_content: {
        flex: 3,
        backgroundColor: '#383',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
